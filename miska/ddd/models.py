import dataclasses
import functools
import types
import typing as t

# Types
_T_RO_DICT = types.MappingProxyType
_T_VALIDATOR = t.Callable[[t.Any], None]
_T_FACTORY = t.Callable[[], None]

# Metadata fields
_M_PRIVATE = "private"
_M_VALIDATOR = "validator"
_M_FIELDS = frozenset((_M_PRIVATE, _M_VALIDATOR))
_RO_DICT = types.MappingProxyType({})

# Flags
_DEFAULT = object()
_INITIALIZED = "_INITIALIZED"
_PASS = lambda *args, **kwargs: None  # noqa


def field(
        *,
        private: bool = _DEFAULT,
        validator: t.Optional[_T_VALIDATOR] = dataclasses.MISSING,
        **kwargs,
) -> dataclasses.Field:
    meta = kwargs.setdefault("metadata", {})
    if not _M_FIELDS.isdisjoint(meta):
        raise ValueError(f"Metadata {_M_FIELDS} can't be defined directly")

    meta.update(private=private, validator=validator)
    return dataclasses.field(**kwargs)


private_field = functools.partial(field, private=True)
public_field = functools.partial(field, private=False)


class BaseModel:
    __privacy_policy__ = True  # NOTE(d.burmistrov): FlagEnum
    __identity_qualifier__: str | tuple[str, ...] = "id"
    __publish_immutables__: str | tuple[str, ...] | None = None

    __INITIALIZED: bool = False

    __AUTO_VIEW: t.ClassVar
    __DEFAULTS: t.ClassVar[tuple[
        _T_RO_DICT[str, t.Any],
        tuple[tuple[str, _T_FACTORY], ...],
    ]] = _RO_DICT, tuple()
    __PRIVATES: t.ClassVar[frozenset[str]] = frozenset()
    __VALIDATORS: t.ClassVar[_T_RO_DICT[str, _T_VALIDATOR]] = _RO_DICT
    __PUBLIC_VIEW_API = (
        "__identity_qualifier__",
        "get_identity_qualifier",
        "get_identity",
        "as_tuple",
        "as_dict",
        "copy",
    )

    def __init_subclass__(cls):
        super(BaseModel, cls).__init_subclass__()
        dataclasses.dataclass(cls)

        validators = {}
        defaults = ({}, [])
        field_names, privates, occupied = set(), set(), set()
        for fld in dataclasses.fields(cls):
            field_names.add(fld.name)
            # NOTE(d.burmistrov): cls.__preprocess_field(fld/meta)
            cls.__discover_default(fld, *defaults)
            cls.__discover_private(privates, occupied, fld.metadata, fld.name)
            cls.__discover_validator(validators, fld.metadata, fld.name)

        _name_conflict = field_names.intersection(occupied)
        if _name_conflict:
            msg = f"Private fields conflict with fields {_name_conflict}"
            raise AttributeError(msg)

        cls.__DEFAULTS = (types.MappingProxyType(defaults[0]),
                          tuple(defaults[1]))
        cls.__PRIVATES = frozenset(privates)
        cls.__VALIDATORS = types.MappingProxyType(validators)
        cls.__AUTO_VIEW = cls.__create_view()

    @classmethod
    def __create_view(cls):
        ns = []
        ns_extra = cls.__publish_immutables__
        if isinstance(ns_extra, str):
            ns = [ns_extra]
        elif ns_extra:
            ns = ns_extra
        ns.extend(cls.__PUBLIC_VIEW_API)

        return dataclasses.make_dataclass(
            cls.__name__,
            ((fld.name, fld.type) for fld in dataclasses.fields(cls)),
            namespace={attr: getattr(cls, attr) for attr in ns},
            frozen=True,
            kw_only=True,
            slots=True,
            module=cls.__module__,
        )

    @classmethod
    def __discover_default(cls, fld, static, factories):
        if fld.default is not dataclasses.MISSING:
            static[fld.name] = fld.default
        elif fld.default_factory is not dataclasses.MISSING:
            factories.append((fld.name, fld.default_factory))

    @classmethod
    def __discover_private(cls, privates, occupied, meta, name):
        private = meta.get(_M_PRIVATE, _DEFAULT)
        if private is _DEFAULT:
            private = cls.__privacy_policy__
        if not private:
            return
        if name[1:2] == "_":
            raise ValueError(f"Private fields must be named publicly: {name}")
        privates.add(name)
        occupied.add(f"_{name}")

    @classmethod
    def __discover_validator(cls, validators, meta, name):
        vlr = meta.get(_M_VALIDATOR, dataclasses.MISSING)
        if vlr is dataclasses.MISSING:
            validators[name] = _PASS
        else:
            validators[name] = vlr

    def __post_init__(self):
        self._validate()
        self.__INITIALIZED = True

    def _apply_validators(self):
        for fld, value in self.as_dict().items():
            self.__VALIDATORS[fld](value)

    def _validate(self) -> None:
        self._apply_validators()
        self.get_identity()

    def _setattr(self, key: str, value):
        if hasattr(self, key):
            self.__VALIDATORS[key](value)
        return super(BaseModel, self).__setattr__(key, value)

    def __setattr__(self, key: str, value):
        # allow to initialize instance during dataclass magic
        if not self.__INITIALIZED:
            return super(BaseModel, self).__setattr__(key, value)
        # allow managing regular private attributes
        elif key.startswith("_") and hasattr(self, key):
            return self._setattr(key, value)

        # magic for inner modification like `self._private_field = 42`
        #   (it converts name into field that marked as private)
        if key.startswith("_"):
            _key = key[1:]
            if _key in self.__PRIVATES:
                return self._setattr(_key, value)

        # magic for restricting private field modifications
        if key in self.__PRIVATES:
            msg = f"Forbidden to modify private attribute: {key}"
            raise AttributeError(msg)

        return self._setattr(key, value)

    def __delattr__(self, item):
        raise AttributeError("Attribute deletion is forbidden")

    @classmethod
    def compute_defaults(cls):
        if not cls.__DEFAULTS:
            return {}

        static, factories = cls.__DEFAULTS
        result = static.copy()
        result.update((fld_name, factory()) for fld_name, factory in factories)
        return result

    @classmethod
    def get_identity_qualifier(cls):
        return cls.__identity_qualifier__

    def get_identity(self) -> None:
        return getattr(self, self.get_identity_qualifier())

    # NOTE(d.burmistrov): override if you need
    def as_view(self):
        return self.__AUTO_VIEW(**self.as_dict())

    as_tuple = dataclasses.astuple
    as_dict = dataclasses.asdict

    copy = dataclasses.replace
