Miska
#####


|

    *"Miska-llaneous code, tools and stuff"*

|


Versioning
**********

`SemVer <http://semver.org/>`__ used for versioning.
For available versions see the repository
`tags <https://gitlab.com/pyctrl/miska/-/tags>`__
and `releases <https://gitlab.com/pyctrl/miska/-/releases>`__.


Authors
*******

-  **Dima Burmistrov** - *Initial work* -
   `pyctrl <https://gitlab.com/pyctrl/>`__


License
*******

This project is licensed under the X11 License (extended MIT) - see the
`LICENSE <https://gitlab.com/pyctrl/miska/-/blob/main/LICENSE>`__ file for details
